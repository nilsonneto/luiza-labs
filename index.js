let express = require('express');
let app = express();
let helper = require('./helpers');
let reports = require('./routes/getAllReports');
let report = require('./routes/getReport');
var md = require('markdown-it')();

app.get('/', function (req, res) {
    helper.getReadme().then(value => res.end(md.render(value))).catch(err => {
        console.log(err);
        res.status(400).end(err.message);
    });
});

app.get('/api/', function (req, res) {
    reports.get().then(data => res.end(JSON.stringify(data))).catch(err => {
        console.log(err);
        res.status(400).end(err.message);
    });
});

app.get('/api/:id', function (req, res) {
    report.getId(req.params.id).then(data => res.end(JSON.stringify(data))).catch(err => {
        console.log(err);
        res.status(400).end(err.message);
    });
});

let index = app.listen(3000, function () {
    let port = index.address().port;
    console.log("App listening at http://localhost:%s", port);
});
