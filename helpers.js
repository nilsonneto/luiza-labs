const fs = require('mz/fs');

async function getReadme() {
    return await fs.readFile(__dirname + "/README.md", 'utf8');
}

async function getLog() {
    return await fs.readFile(__dirname + "/quake/games.log", 'utf8');
}

module.exports = {
    getReadme: getReadme,
    getLog: getLog
};