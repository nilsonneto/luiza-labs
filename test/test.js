const expect = require("chai").expect;
const request = require("request");
let helpers = require("../helpers");
let reports = require("../routes/getAllReports");
let report = require("../routes/getReport");

describe("Auxiliary functions", function () {
    describe("README Getter", function () {
        it("Reads the README.md file and returns it when ready", function () {
            let readme = helpers.getReadme();
            expect(readme).to.be.ok;
        });
    });
    
    describe("Log Getter", function () {
        it("Reads the log file and returns it when ready", function () {
            let log = helpers.getLog();
            expect(log).to.be.ok;
        });
    });
});

describe("Report tests", function () {
    describe("Get all reports", function () {
        var url = "http://localhost:3000/api";
        it("Returns status 200 without /", function () {
            request(url, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });
        
        it("Returns status 200 with /", function () {
            request(url + "/", function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });
    
        it("Checks if there is at least one report", function () {
            request(url, function(error, response, body) {
                expect(JSON.parse(body)).to.have.property('game_1');
            });
        });
    });
    
    describe("Get specific report", function () {
        var goodUrl = "http://localhost:3000/api/1";
        var maybeUrl = "http://localhost:3000/api/41";
        var badUrl = "http://localhost:3000/api/eeeeeeeee";
    
        it("Check if the first report exists", function () {
            request(goodUrl, function(error, response, body) {
                expect(response.statusCode).to.equal(200);
            });
        });
    
        it("Check if the random report exists", function () {
            request(maybeUrl, function(error, response, body) {
                expect(response.statusCode).to.be.oneOf([200, 400]);
            });
        });
    
        it("Check if a bad url reports error", function () {
            request(badUrl, function(error, response, body) {
                expect(response.statusCode).to.equal(400);
            });
        });
    });
});