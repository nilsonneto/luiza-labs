const helper = require('../helpers');
const world = "<world>";
const gameReg = new RegExp('(?<=InitGame)(.*?)(?=ShutdownGame)', 'gms');
const playerReg = new RegExp('(?<=ClientUserinfoChanged.*?n\\\\).*?(?=\\\\t\\\\0)', 'gi');

// If one the player's name starts or ends with the word 'killed', it will break the parsing
// since it will not *easily* possible to determine when the killer's name ends and when the victim's start
const killReg = new RegExp('(?<=Kill:.*?: )(.*) killed (.*)(?= by \\w*?)', 'gim');

function fillMap(game, killMap) {
    let kills = killReg.exec(game);
    let total_kills = 0;
    while (kills != null) {
        let killer = kills[1];
        let victim = kills[2];
        if (killer === world) {
            if (killMap[victim]) {
                killMap[victim] -= 1;
            } else {
                killMap[victim] = -1;
            }
        } else {
            if (killMap[killer]) {
                killMap[killer] += 1;
            } else {
                killMap[killer] = 1;
            }
        }
        total_kills++;
        kills = killReg.exec(game);
    }
    return total_kills;
}

function generateReport(games){
    let game_index = 0;
    return games.reduce((allGames, game) => {
        game_index++;
        let players = [...new Set(game.match(playerReg))];
        let killMap = players.reduce((allPlayers, player) => {
            allPlayers[player] = 0;
            return allPlayers;
        }, {});
        let total_kills = fillMap(game, killMap);
        let gameId = "game_" + game_index;
        allGames[gameId] = {
            total_kills: total_kills,
            players: players,
            kills: killMap
        };
        return allGames;
    }, {});
}

async function getAllReports(){
    return helper.getLog().then(value => {
        let games = value.match(gameReg);
        return generateReport(games);
    });
}

module.exports = {
    get: getAllReports
};
