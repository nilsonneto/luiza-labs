const reports = require('./getAllReports');

async function getReportById(id){
    return reports.get().then(data => {
        let gameId = "game_" + id;
        if (data[gameId])
        {
            return data[gameId];
        }else{
            throw new Error("ID not found");
        }
    });
}

module.exports = {
    getId: getReportById
};