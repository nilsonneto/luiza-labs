# Luiza Labs - Quake Challenge

This challenge was built using node.js, so there are some requirements to run it:

## Requirements
* Node.js v11.14.0: <https://nodejs.org/en/download/>
* A text editor
* [Optional] If you want the server to auto-reload when a file changes
    * Use nodemon: <https://github.com/remy/nodemon>
* Clone the repository: <https://bitbucket.org/nilson_neto/luiza-labs/>

## Running
After installing the requirements, follow these steps:
* Open a terminal where the repository was cloned
* Run `npm install`
    * This will install all dependencies
* Run `node index.js` or `nodemon index.js`
    * This will start the server, making the API accessible at <http://localhost:3000>


## API Endpoints

Path|Description
---|:---:
`/api/`|Returns all game reports
`/api/:id`|Returns a specific game report

